## CMS App

### Build the Docker containers
    git clone https://bitbucket.org/k0d3r/cms-app/src/main/ app
    cd app/docker
    docker-compose up -d --build

### Install Composer dependencies
    docker container exec php composer install

### Migrate the database
    docker container exec php php artisan migrate

### Compile front-end assets
    docker container exec node npm run dev
