<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    private $validationRules = [
        'title' => 'required|max:255',
        'slug' => ['required', 'max:255'],
        'content' => 'required',
        'menu_index' => 'numeric',
        'published' => 'boolean'
    ];

    private $message = 'Post %s successfully';

    public function __construct()
    {
        View::share('menuItems', Post::getMenuItems());

        $this->middleware('auth')->except('view');
    }

    public function view($slug = null)
    {
        $post = is_null($slug) ? Post::first() : Post::where('slug', $slug)->first();

        return view('posts.view')->with('post', $post);
    }

    public function index()
    {
        $posts = Post::all();

        return view('posts.index')->with('posts', $posts);
    }

    public function create()
    {
        return view('posts.upsert')->with('post', new Post());
    }

    public function store(Request $request)
    {
        array_push($this->validationRules['slug'], 'unique:posts');

        $request->validate($this->validationRules);

        Post::create($request->all());

        return redirect()->route('posts.index')->with('message', sprintf($this->message, 'created'));
    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.upsert')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        array_push($this->validationRules['slug'], "unique:posts,slug,$id,id");

        $request->validate($this->validationRules);

        Post::find($id)->update($request->all());

        return redirect()->route('posts.index')->with('message', sprintf($this->message, 'updated'));
    }

    public function destroy($id)
    {
        Post::find($id)->delete();

        return redirect()->route('posts.index')->with('message', sprintf($this->message, 'deleted'));
    }
}
