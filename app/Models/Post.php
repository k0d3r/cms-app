<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
        'title',
        'slug',
        'content',
        'published',
        'menu_index'
    ];

    public static function getMenuItems()
    {
        return Post::where('published', 1)->select('title', 'slug')->orderBy('menu_index', 'asc')->get();
    }
}
