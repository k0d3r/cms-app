@extends('layouts.cms-app')

@section('title')
    {{ $post ? "Viewing $post->title" : 'Post not found' }}
@endsection

@section('content')

    <a class="btn btn-secondary mb-4" href="{{ route('posts.index') }}"><i class="fas fa-arrow-left"></i> All Posts</a>
            
    @if($post)
        <table class="table table-bordered">
            <tr>
                <td width="200"><strong>Title:</strong></td>
                <td>{{ $post->title }}</td>
            </tr>
            <tr>
                <td><strong>Content:</strong></td>
                <td>{{ $post->content }}</td>
            </tr>
            <tr>
                <td><strong>Published:</strong></td>
                <td>{{ $post->published ? 'Yes' : 'No' }}</td>
            </tr>
            <tr>
                <td><strong>Date Created:</strong></td>
                <td>{{ date_format($post->created_at, 'jS M Y') }}</td>
            </tr>
        </table>
    @else
        <h2>Post not found.</h2>
    @endif

@endsection