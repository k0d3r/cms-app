@extends('layouts.cms-app')

@section('title')
    {{ $post ? $post->title : 'Post not found' }}
@endsection

@section('content')
    @if($post)
        <h1>{{ $post->title }}</h1>
        <p class="text-secondary">Last Updated: {{ date_format($post->updated_at, 'jS F Y') }}</p>
        {!! Str::of($post->content)->markdown() !!}
    @else
        <h2>The post you requested could not be found.</h2>
    @endif
@endsection
