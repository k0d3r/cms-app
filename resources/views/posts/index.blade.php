@extends('layouts.cms-app')

@section('title')
    Posts
@endsection

@section('content')

    <a class="btn btn-success mb-4" href="{{ route('posts.create') }}">Create New Post</a>

    @if($posts->isNotEmpty())
        <table id="posts" class="table table-striped table-bordered table-hover w-100">
            <thead class="thead-dark">
                <tr>
                    <th width="5%">ID</th>
                    <th width="10%">Title</th>
                    <th width="10%">Slug</th>
                    <th width="33%">Content</th>
                    <th width="5%">Published</th>
                    <th width="7%">Menu Index</th>
                    <th width="10%">Created</th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->slug }}</td>
                        <td>{{ Str::of($post->content)->limit(400) }}</td>
                        <td>{{ $post->published ? 'Yes' : 'No' }}</td>
                        <td>{{ $post->menu_index }}</td>
                        <td>{{ date_format($post->created_at, 'jS M Y') }}</td>
                        <td>
                            <div class="dropdown">
                                
                                <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="actions-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                
                                <div class="dropdown-menu" aria-labelledby="actions-dropdown">
                                    <a class="dropdown-item" href="{{ route('posts.view', $post->slug) }}"><i class="fa fas fa-external-link-alt"></i> View</a>
                                    
                                    <a class="dropdown-item" href="{{ route('posts.show', $post->id) }}"><i class="fa far fa-eye"></i> Show more</a>

                                    <a class="dropdown-item" href="{{ route('posts.edit', $post->id) }}"><i class="fa fas fa-edit"></i> Edit</a>
                                    
                                    <form action="{{ route('posts.destroy', $post->id) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="dropdown-item"><i class="fa far fa-trash-alt"></i> Delete</button>
                                    </form>
                                </div>

                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <h2>No posts to display.</h2>
        <p>Would you like to <a href="{{ route('posts.create') }}">create a new post</a>?</p>
    @endif

@endsection
