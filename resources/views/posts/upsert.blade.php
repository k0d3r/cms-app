@extends('layouts.cms-app')

@if($post)
    @section('title')
        {{ $post->exists ? 'Edit Post' : 'Add Post' }}
    @endsection

    @section('content')
        <div class="row">
            
            <div class="col-lg-12">
                <a class="btn btn-secondary mb-4" href="{{ route('posts.index') }}"><i class="fas fa-arrow-left"></i> All Posts</a>
                <h2>{{ $post->exists ? 'Edit Post' : 'Create a New Post' }}</h2>
            </div>

            @if($errors->any())
                <div class="col-sm-6">
                    <div class="alert alert-danger">
                        <p><strong>There were some problems saving your content:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

        </div>

        <form action="{{ $post->exists ? route('posts.update', $post->id) : route('posts.store') }}" method="POST">
            
            @csrf

            @if($post->exists)
                @method('PATCH')
            @endif

            <div class="row">
                
                <div class="col-sm-6">
                    
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="{{ old('title', $post->title) }}" class="form-control" placeholder="Title">
                    </div>

                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" id="slug" value="{{ old('slug', $post->slug) }}" class="form-control" placeholder="Slug">
                    </div>

                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" rows="15" class="form-control" placeholder="Content">{{ old('content', $post->content) }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="menu-index">Menu Index</label>
                        <select name="menu_index" id="menu-index" class="form-control">
                            @foreach (range(1, 10) as $menuIndex)
                                <option value="{{ $menuIndex }}" {{ ($menuIndex == old('menu-index', $post->menu_index)) ? 'selected' : '' }}>{{ $menuIndex }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-check form-group">
                        <input type="checkbox" name="published" id="published" value="1" {{ old('published', $post->published) || !$post->exists ? 'checked' : '' }} class="form-check-input">
                        <label for="published" class="form-check-label">Published</label>
                    </div>

                    <button type="submit" class="btn btn-success">Save</button>

                </div>

            </div>

        </form>
    @endsection
@else
    @section('title')
        Post not found
    @endsection

    @section('content')
        <h2>Post not found.</h2>
    @endsection
@endif
