<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        
        <meta charset="utf-8">
        
        <meta name="viewport" content="initial-scale=1, shrink-to-fit=no">

        <title>{{ config('app.name') }} | @yield('title')</title>

        <link rel="icon" type="image/x-icon" href="/favicon.ico">

        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
              integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
              crossorigin="anonymous">

        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
              integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
              crossorigin="anonymous">

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap">

        <link rel="stylesheet" href="{{ asset('css/cms-app.css') }}">

    </head>

    <body>
        
        <header>

            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            
                <a class="navbar-brand" href="/">
                    <img id="logo" src="/img/logo.png" alt="GeoPal">
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav-dropdown" aria-controls="navbar-nav-dropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbar-nav-dropdown">
                    
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        @isset($menuItems)
                            @foreach($menuItems as $menuItem)
                                <li class="nav-item">
                                    <a href="{{ route('posts.view', $menuItem->slug) }}" class="nav-link">{{ $menuItem->title }}</a>
                                </li>
                            @endforeach
                        @endisset
                    </ul>
                    
                    <ul class="navbar-nav my-2 my-lg-0">
                         @if(Route::has('login'))
               
                            @auth
                                <li class="nav-item">
                                    <a href="{{ route('posts.index') }}" class="nav-link">Dashboard</a>
                                </li>
                                
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <li class="nav-item">
                                        <button type="submit" class="logout btn nav-link">Logout</button>
                                    </li>
                                </form>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('login') }}" class="nav-link">Login</a>
                                </li>

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a href="{{ route('register') }}" class="nav-link">Register</a>
                                    </li>
                                @endif
                            @endif

                        @endif
                    </ul>

                </div>
            
            </nav>
            
        </header>

        <div class="container-fluid">

            <article>
                @if (Session::has('message'))
                    <div class="alert alert-success mb-4"><i class="fa fas fa-exclamation-triangle"></i> {{ Session::get('message') }}</div>
                @endif

                @yield('content')
            </article>

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
                integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
                crossorigin="anonymous">
        </script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
                crossorigin="anonymous">
        </script>

    </body>

</html>
